/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.common.supcan.common;

import java.util.List;

import com.empire.emsite.common.supcan.common.fonts.Font;
import com.empire.emsite.common.supcan.common.properties.Properties;
import com.empire.emsite.common.utils.IdGen;
import com.google.common.collect.Lists;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 硕正Common
 * 
 * @author WangZhen
 * @version 2013-11-04
 */
/**
 * 类Common.java的实现描述：硕正Common
 * 
 * @author arron 2017年10月30日 下午3:22:54
 */
public class Common {

    /**
     * 属性对象
     */
    @XStreamAlias("Properties")
    protected Properties properties;

    /**
     * 字体对象
     */
    @XStreamAlias("Fonts")
    protected List<Font> fonts;

    public Common() {
        properties = new Properties(IdGen.uuid());
        fonts = Lists.newArrayList(new Font("宋体", "134", "-12"), new Font("宋体", "134", "-13", "700"));
    }

    public Common(Properties properties) {
        this();
        this.properties = properties;
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    public List<Font> getFonts() {
        return fonts;
    }

    public void setFonts(List<Font> fonts) {
        this.fonts = fonts;
    }

}
