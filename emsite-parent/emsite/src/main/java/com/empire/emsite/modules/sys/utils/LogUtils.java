/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.sys.utils;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.method.HandlerMethod;

import com.empire.emsite.common.config.MainConfManager;
import com.empire.emsite.common.utils.CacheUtils;
import com.empire.emsite.common.utils.Exceptions;
import com.empire.emsite.common.utils.SpringContextHolder;
import com.empire.emsite.common.utils.StringUtils;
import com.empire.emsite.modules.sys.entity.Log;
import com.empire.emsite.modules.sys.entity.Menu;
import com.empire.emsite.modules.sys.entity.User;
import com.empire.emsite.modules.sys.facade.LogFacadeService;
import com.empire.emsite.modules.sys.facade.SystemFacadeService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * 类LogUtils.java的实现描述：字典工具类
 * 
 * @author arron 2017年10月30日 下午7:20:53
 */
public class LogUtils {

    public static final String         CACHE_MENU_NAME_PATH_MAP = "menuNamePathMap";

    private static LogFacadeService    logService               = SpringContextHolder.getBean(LogFacadeService.class);
    private static SystemFacadeService systemService            = SpringContextHolder
                                                                        .getBean(SystemFacadeService.class);
    private static ThreadPoolExecutor  executor                 = new ThreadPoolExecutor(10, 20, 0L,
                                                                        TimeUnit.MILLISECONDS,
                                                                        new ArrayBlockingQueue<Runnable>(2000));

    /**
     * 保存日志
     */
    public static void saveLog(HttpServletRequest request, String title) {
        saveLog(request, null, null, title);
    }

    /**
     * 保存日志
     */
    public static void saveLog(HttpServletRequest request, Object handler, Exception ex, String title) {
        User user = UserUtils.getUser();
        if (user != null && user.getId() != null) {
            Log log = new Log();
            log.setCurrentUser(user);
            log.setTitle(title);
            log.setType(ex == null ? Log.TYPE_ACCESS : Log.TYPE_EXCEPTION);
            log.setRemoteAddr(StringUtils.getRemoteAddr(request));
            log.setUserAgent(request.getHeader("user-agent"));
            log.setRequestUri(request.getRequestURI());
            log.setParams(request.getParameterMap());
            log.setMethod(request.getMethod());
            // 异步保存日志
            //new SaveLogThread(log, handler, ex).start();
            //改为线程池记录日志，防止访问量大出现服务器崩溃情况
            executor.execute(new SaveLogThread(log, handler, ex));
        }
    }

    /**
     * 保存日志线程
     */
    public static class SaveLogThread extends Thread {

        private Log       log;
        private Object    handler;
        private Exception ex;

        public SaveLogThread(Log log, Object handler, Exception ex) {
            super(SaveLogThread.class.getSimpleName());
            this.log = log;
            this.handler = handler;
            this.ex = ex;
        }

        @Override
        public void run() {
            // 获取日志标题
            if (StringUtils.isBlank(log.getTitle())) {
                String permission = "";
                if (handler instanceof HandlerMethod) {
                    Method m = ((HandlerMethod) handler).getMethod();
                    RequiresPermissions rp = m.getAnnotation(RequiresPermissions.class);
                    permission = (rp != null ? StringUtils.join(rp.value(), ",") : "");
                }
                log.setTitle(getMenuNamePath(log.getRequestUri(), permission));
            }
            // 如果有异常，设置异常信息
            log.setException(Exceptions.getStackTraceAsString(ex));
            // 如果无标题并无异常日志，则不保存信息
            if (StringUtils.isBlank(log.getTitle()) && StringUtils.isBlank(log.getException())) {
                return;
            }
            // 保存日志信息
            logService.save(log);
        }
    }

    /**
     * 获取菜单名称路径（如：系统设置-机构用户-用户管理-编辑）
     */
    public static String getMenuNamePath(String requestUri, String permission) {
        String href = StringUtils.substringAfter(requestUri, MainConfManager.getAdminPath());
        @SuppressWarnings("unchecked")
        Map<String, String> menuMap = (Map<String, String>) CacheUtils.get(CACHE_MENU_NAME_PATH_MAP);
        if (menuMap == null) {
            menuMap = Maps.newHashMap();
            List<Menu> menuList = systemService.findAllMenu();
            for (Menu menu : menuList) {
                // 获取菜单名称路径（如：系统设置-机构用户-用户管理-编辑）
                String namePath = "";
                if (menu.getParentIds() != null) {
                    List<String> namePathList = Lists.newArrayList();
                    for (String id : StringUtils.split(menu.getParentIds(), ",")) {
                        if (Menu.getRootId().equals(id)) {
                            continue; // 过滤跟节点
                        }
                        for (Menu m : menuList) {
                            if (m.getId().equals(id)) {
                                namePathList.add(m.getName());
                                break;
                            }
                        }
                    }
                    namePathList.add(menu.getName());
                    namePath = StringUtils.join(namePathList, "-");
                }
                // 设置菜单名称路径
                if (StringUtils.isNotBlank(menu.getHref())) {
                    menuMap.put(menu.getHref(), namePath);
                } else if (StringUtils.isNotBlank(menu.getPermission())) {
                    for (String p : StringUtils.split(menu.getPermission())) {
                        menuMap.put(p, namePath);
                    }
                }

            }
            CacheUtils.put(CACHE_MENU_NAME_PATH_MAP, menuMap);
        }
        String menuNamePath = menuMap.get(href);
        if (menuNamePath == null) {
            for (String p : StringUtils.split(permission)) {
                menuNamePath = menuMap.get(p);
                if (StringUtils.isNotBlank(menuNamePath)) {
                    break;
                }
            }
            if (menuNamePath == null) {
                return "";
            }
        }
        return menuNamePath;
    }

}
